#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
    srand(time(0));

    int size = 0;

    cout << "Enter size of array:";

    cin >> size;

    int* arr;

    arr = new int[size];

    for (int i = 0; i < size; i++)
        arr[i] = rand() % 100;

    cout << "Before sorting :";

    for (int i = 0; i < size; i++)
        cout << arr[i] << " ";

    cout << "\n";

    for (int i = 0; i < size; i++)
    {
        for (int j = i + 1; j < size; j++)
        {

           
            if (arr[i] >= 0 && arr[j] >= 0 &&
                arr[i] % 2 == 0 &&
                arr[j] % 2 != 0)
            {

              
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;

           
                arr[j] = -arr[j];

                break;
            }
            else if (arr[i] >= 0 && arr[j] >= 0 &&
                arr[i] % 2 != 0 &&
                arr[j] % 2 == 0)
            {

               
                int tmp = arr[i];
                arr[i] = arr[j];
                arr[j] = tmp;

            
                arr[j] = -arr[j];

                break;
            }
        }
    }
  
    for (int i = 0; i < size; i++)
        arr[i] = abs(arr[i]);

        
    
     

    cout << "After sorting :";

    for (int i = 0; i < size; i++)
        cout << arr[i] << " ";

    delete[] arr;

    return 0;

}