// (a + ib)*(c + id) = r + is
#include <iostream>
#include <cmath>
#include <complex>
using namespace std;

int Karatsuba(int X, int Y) {
    int Xn{}, Yn{}; // ����� ����� (658=100, 25=10)
    for (int i = 0; i < floor(log10(abs(X))) + 1; i++) {

        Xn = pow(10 , i);
    }
for (int i = 0; i < floor(log10(abs(Y))) + 1; i++) {

        Yn = pow(10 , i);
    }
    int  XfirstDigit = floor(X / Xn); 
    int  XotherDigits = X % Xn;
    int  YfirstDigit = floor(Y / Yn);
    int  YotherDigits = Y % Yn;
    int C = (XfirstDigit + XotherDigits) * (YfirstDigit + YotherDigits) - XfirstDigit * YfirstDigit - XotherDigits * YotherDigits;
    return XfirstDigit * YfirstDigit * Xn * Yn + C * Xn + XotherDigits * YotherDigits;
}

int main() {
   int a, b, c, d ,r ,s;
  cout << "a= "; cin >> a;
  cout << "b= "; cin >> b;
  cout << "c= "; cin >> c;
  cout << "d= "; cin >> d;
  // a*c+ a*i*d + c*i*b +i*i*b*b = (a*c + a*i*d) + (-b*b + c*i*b)
  complex<int> firstPhrase(Karatsuba(a,c), Karatsuba(a,d));
  complex<int> secondPhrase(-Karatsuba(b,b), Karatsuba(c,b));
 

  cout << "r + is = " <<  real(firstPhrase+secondPhrase) <<" + i" << imag(firstPhrase + secondPhrase);
   
    return 0;
}