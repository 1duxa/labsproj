#include<iostream>
#include<iomanip>
#include<cmath>
#include<algorithm>

using namespace std;

void Create(float* a, const int size)
{
    if (size % 2 == 0) {
        for (int i = 0; i < size / 2; i++) {
             a[i + size / 2] = -(float)(rand()) / (float)(rand() % 100);
             a[i] = (float)(rand()) / (float)(rand() % 100);
        }
    }
    else {
        for (int i = 0; i <= size / 2; i++) {
             a[i + size / 2] = -(float)(rand()) / (float)(rand() % 100);
             a[i] = (float)(rand()) / (float)(rand() % 100);
        }
    }
    }

void Print(float* a, const int size)
{
    cout << "a[ ";
    for (int i = 0; i < size; ++i)
        cout << a[i] << ", ";
    cout << "]" << endl;
}

void Dobutok(float* a, const int size)
{
    float min = 0;
    float max = 0;
    float sum=1;
    int mini{},maxi{};
    for (int i = 0; i < size; i++) {
        if (min > a[i]) {
            min = a[i];
            mini = i;
        }
    }
    for (int i = 0; i < size; i++) {
        if (max < a[i]) {
            max = a[i];
            maxi = i;
        }
    }
    cout << "\n MIN " << mini << "\n MAX " << maxi;
    for (int i = 0; i < size; i++) {
        if(i >= maxi && i<=mini) {
            sum *= a[i];
        
        }
     
        
    }
    cout << "\n Dobytok = " << sum;
}
void Sort(float* a, const int size)
{
   
        int* arr = (int*)a;
        int l = 0, r = size - 1;
        int k = 0;

        while (l < r)
        {

            while (arr[l]% 2 != 0)
            {
                l++;
                k++;
            }

      
            while (arr[r] % 2 == 0 && l < r)
                r--;

            if (l < r)
                swap(arr[l], arr[r]);
        }


        sort(arr + k, arr + size);
        for (int i = 0; i < size; i++)
            cout << ", " << arr[i];
}


int main()
{
    srand((unsigned)time(NULL));
    int size = 0;
    cout << "Size of array:"; cin >> size;

    float* array;
    array = new float[size];

    Create(array, size);
    Print(array, size);
    Dobutok(array, size);
    Sort(array, size);

    delete[] array;
    return 0;
}